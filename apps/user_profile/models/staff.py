from django.contrib.auth.models import User
from django.db import models
from model_utils.models import TimeStampedModel


class StaffModel(TimeStampedModel):
    """
    Model for the personal profile.
    """
    TEACHER = "teacher"
    DIRECTOR = "director"
    COORDINATOR = "coordinator"
    WORKER = "worker"
    ADMINISTRATIVE_PERSONAL = "administrative_personal"

    TYPES = (
        (TEACHER, "Teacher"),
        (DIRECTOR, "Director"),
        (COORDINATOR, "Coordinator"),
        (WORKER, "Worker"),
        (ADMINISTRATIVE_PERSONAL, "Administrative Personal")
    )

    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True
    )
    ci = models.PositiveIntegerField(unique=True)
    is_admin = models.BooleanField(default=False)
    type = models.CharField(
        max_length=25,
        choices=TYPES,
        default=TEACHER
    )

    def __str__(self):
        return "{} - {} {}".format(self.type, self.user.first_name, self.user.last_name)
