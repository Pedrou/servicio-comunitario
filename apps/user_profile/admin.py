from django.contrib import admin
from .models import StaffModel


class StaffAdmin(admin.ModelAdmin):
    pass


admin.site.register(StaffModel, StaffAdmin)
