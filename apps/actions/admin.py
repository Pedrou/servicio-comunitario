from django.contrib import admin
from .models import ActionsModel, StaffActionsModel


class ActionsAdmin(admin.ModelAdmin):
    pass


class StaffActionsAdmin(admin.ModelAdmin):
    pass


admin.site.register(ActionsModel, ActionsAdmin)
admin.site.register(StaffActionsModel, StaffActionsAdmin)
