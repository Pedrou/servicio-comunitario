from django.db import models
from model_utils.models import TimeStampedModel


class ActionsModel(TimeStampedModel):
    """
    Model for Actions
    """

    ARRIVAL = 'arrival'
    DEPARTURE = 'departure'
    LEAVE_PERMISSION = 'leave_permission'

    TYPES = (
        (ARRIVAL, 'Arrival'),
        (DEPARTURE, 'Departure'),
        (LEAVE_PERMISSION, 'Leave Permission')
    )

    duration = models.DurationField(null=True)
    description = models.TextField()
    type = models.CharField(
        max_length=20,
        choices=TYPES,
        default=ARRIVAL
    )
