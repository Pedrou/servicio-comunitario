from django.db import models
from apps.actions.models import ActionsModel
from apps.user_profile.models import StaffModel


class StaffActionsModel(models.Model):
    staff = models.ForeignKey(StaffModel, null=True, on_delete=models.PROTECT)
    action = models.ForeignKey(ActionsModel, null=True, on_delete=models.CASCADE)
