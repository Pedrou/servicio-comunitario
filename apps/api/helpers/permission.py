from rest_framework import permissions
from apps.user_profile.models import StaffModel


def is_admin(user):

    try:
        staff = StaffModel.objects.get(
            user=user.id
        )
        return staff.is_admin

    except:
        return False


class IsAdmin(permissions.BasePermission):
    """
    This Class returns if the Requesting User model Instance is a valid Manager
    and if it is Authenticated due to the validation criterial described
    """

    def has_permission(self, request, view):
        """
        This function Evluate if the user on the request is authenticated due
        to the defined Authentication method Defined on the Django Settings and
        if the Requesting user is a manager due to certain criterial defined
        in the is_manager() method.
        :param request: request object.
        :param view:
        :return: Returns True if the Criterial is Satisfied or False if it is
                 not.
        """

        return request.user.is_authenticated and is_admin(request.user)
