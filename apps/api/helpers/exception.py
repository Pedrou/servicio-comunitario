from rest_framework import serializers
from rest_framework import status


class CustomAPIException(serializers.ValidationError):
    status_code = status.HTTP_400_BAD_REQUEST
    default_code = "error"

    def __init__(self, msg, code, status_code=None, fields=None):
        # TODO pending change status_code to code for v2 API
        self.detail = {"error": {"status_code": code, "message": msg}}

        if fields is not None:
            self.detail["error"]["fields"] = fields

        if status_code is not None:
            self.status_code = status_code
