from django.urls import path, include
from apps.api.views import StaffSettingViewSet
from rest_framework import routers

router = routers.SimpleRouter()
router.register(
    r"staff_setting", StaffSettingViewSet, base_name="staff_setting"
)

urlpatterns = [
    path("", include(router.urls)),
]
