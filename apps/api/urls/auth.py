from django.contrib import admin
from django.urls import path, include
from apps.api.views import CustomAuthToken

urlpatterns = [
    path('auth/', CustomAuthToken.as_view()),
]
