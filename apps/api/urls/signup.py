from django.contrib import admin
from django.urls import path, include
from apps.api.views import SignUpViewSet
from rest_framework import routers

router = routers.SimpleRouter()
router.register(
    r"signup", SignUpViewSet, base_name="signup"
)

urlpatterns = [
    path("", include(router.urls)),
]
