from django.urls import include, path

urlpatterns = [
    path("", include("apps.api.urls.auth")),
    path("", include("apps.api.urls.signup")),
    path("", include("apps.api.urls.staff")),
    path("", include("apps.api.urls.actions")),
    path("", include("apps.api.urls.staff_setting")),
]
