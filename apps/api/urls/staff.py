from django.contrib import admin
from django.urls import path, include
from apps.api.views import StaffView
from rest_framework import routers

router = routers.SimpleRouter()
router.register(
    r"staff", StaffView, base_name="staff"
)

urlpatterns = [
    path("", include(router.urls)),
]
