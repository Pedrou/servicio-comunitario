# from django.conf import settings
from django.contrib.auth import user_logged_in
from django.contrib.auth.models import User
from rest_framework import mixins, serializers, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from apps.api.helpers import CustomAPIException
from apps.api.serializers.signup import SingUpSerializer
# from apps.user_profile.models import StaffModel


class SignUpViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin):

    serializer_class = SingUpSerializer
    queryset = User.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = SingUpSerializer(
            data=request.data, context={"request": request}
        )

        try:
            serializer.is_valid(raise_exception=True)
        except serializers.ValidationError as e:
            errors = dict()

            if "error" in e.args[0]:
                raise e
            elif "password" in e.args[0]:
                if "error" in e.args[0]["password"]:
                    raise CustomAPIException(
                        e.args[0]["password"]["error"]["message"],
                        e.args[0]["password"]["error"]["status_code"]
                    )

            for error in e.args[0].items():
                if error[1][0].code == "required":
                    if "required" not in errors:
                        errors["required"] = list()

                    errors["required"].append(
                        {"field": error[0], "message": error[1][0]}
                    )

                elif error[1][0].code == "blank":
                    if "blank" not in errors:
                        errors["blank"] = list()

                    errors["blank"].append(
                        {"field": error[0], "message": error[1][0]}
                    )

                elif error[1][0].code == "invalid":
                    if "invalid" not in errors:
                        errors["invalid"] = list()

                    errors["invalid"].append(
                        {"field": error[0], "message": error[1][0]}
                    )

                elif error[1][0].code == "min_length":
                    if "min_length" not in errors:
                        errors["min_length"] = list()

                    errors["min_length"].append(
                        {"field": error[0], "message": error[1][0]}
                    )

                elif error[1][0].code == "max_length":
                    if "max_length" not in errors:
                        errors["max_length"] = list()

                    errors["max_length"].append(
                        {"field": error[0], "message": error[1][0]}
                    )

                if error[1][0].code == "null":
                    if "null" not in errors:
                        errors["null"] = list()

                    errors["null"].append(
                        {"field": error[0], "message": error[1][0]}
                    )

            if "blank" in errors:
                raise CustomAPIException(
                    "{} can't be empty.".format(errors["blank"]),
                    1005,
                    fields=errors["blank"]
                )

            if "required" in errors:
                raise CustomAPIException(
                    "{} are required.".format(errors["required"]),
                    1006,
                    fields=errors["required"]
                )

            if "min_length" in errors:
                raise CustomAPIException(
                    "{} are too short.".format(errors["min_length"]),
                    1007,
                    fields=errors["min_length"]
                )

            if "max_length" in errors:
                raise CustomAPIException(
                    "{} are too long.".format(errors["max_length"]),
                    1008,
                    fields=errors["max_length"]
                )

            if "invalid" in errors:
                raise CustomAPIException(
                    "The email is invalid.".format(errors["invalid"]),
                    1009,
                    fields=errors["invalid"]
                )

            if "null" in errors:
                raise CustomAPIException(
                    "{} isn't valid.".format(errors["null"]),
                    1010,
                    fields=errors["null"]
                )

        serializer.save()

        # Generate uuid_b64 and token
        try:
            # Deactivating previous token for the user.
            user = User.objects.get(
                email__iexact=request.data["email"].lower()
            )
            user_logged_in.send(
                sender=user.__class__, request=request, user=user
            )

        except Exception as err:
            CustomAPIException(
                "Something went wrong registering user",
                1011,
            )

        return Response(
            {
                "message":
                    "It's OK"
            },
            status=200,
        )
