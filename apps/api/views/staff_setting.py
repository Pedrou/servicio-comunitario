from django.conf import settings
from django.contrib.auth import user_logged_in
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from rest_framework import serializers, status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from apps.api.helpers.permission import IsAdmin
from apps.api.serializers import StaffSerializer, StaffSettingSerializer


class StaffSettingViewSet(viewsets.GenericViewSet):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = StaffSerializer

    @action(
        methods=["put"], detail=False, url_path="setting", url_name="staff_setting"
    )
    def staff_setting(self, request):
        serializer = StaffSettingSerializer(
            request.user,
            request.data,
            partial=True,
            context={"request": request},
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"message": "Staff Setting Changed"}, status=200)
