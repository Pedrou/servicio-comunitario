from rest_framework import mixins, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from apps.user_profile.models import StaffModel
from apps.api.helpers.permission import IsAdmin
from apps.api.serializers import StaffSerializer


class StaffView(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = StaffSerializer

    def get_queryset(self):
        type = self.request.query_params.get("type", None)
        is_admin = self.request.query_params.get("is_admin", None)

        filters = {
            "type": type,
            "is_admin": is_admin
        }

        if filters["is_admin"] is not None:
            filters["is_admin"] = True if is_admin == "true" else False

        return StaffModel.objects.filter(
            **dict(
                (key, value)
                for key, value in filters.items()
                if value is not None
            )
        )


