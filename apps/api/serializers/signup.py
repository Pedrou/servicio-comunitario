from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from rest_framework import serializers, status
from apps.user_profile.models import StaffModel
from apps.api.helpers import CustomAPIException


class SingUpSerializer(serializers.Serializer):

    """
    This handles the validation process of all the data that comes in the
    request to create the required model instances for the 1st SingUp
    """

    email = serializers.EmailField(
        required=True,
        write_only=True,
        error_messages={
            "blank": "This Field can't be empty",
            "required": "This field is required",
            "invalid": "The email is invalid",
        },
    )

    password = serializers.CharField(
        write_only=True,
        required=True,
        min_length=8,
        max_length=52,
        error_messages={
            "blank": "This Field can't be empty",
            "required": "This field is required",
            "min_length": "The password is too short < 8",
            "max_length": "The password is too long > 52",
        },
    )

    first_name = serializers.CharField(
        required=True,
        write_only=True,
        min_length=2,
        max_length=25,
        error_messages={
            "blank": "This Field can't be empty",
            "required": "This field is required",
            "min_length": "The first_name is too short < 2",
            "max_length": "The first_name is too long > 25",
        },
    )

    last_name = serializers.CharField(
        required=True,
        write_only=True,
        min_length=2,
        max_length=25,
        error_messages={
            "blank": "This Field can't be empty",
            "required": "This field is required",
            "min_length": "The last_name is too short < 2",
            "max_length": "The last_name is too long > 25",
        },
    )

    ci = serializers.CharField(
        max_length=30,
        required=True,
        write_only=True,
        error_messages={
            "blank": "This Field can't be empty",
            "required": "This field is required",
            "max_length": "The field is too long  > 10",
        },
    )

    is_admin = serializers.BooleanField(
        required=False,
        write_only=True,

    )

    type = serializers.CharField(
        required=False
    )

    def validate(self, attrs):

        """
        This function validates the data that comes in hand from the request
        to validate it in the serialziers.
        :param attrs:  attributes passed to the serialaizer
        :return: attrs
        """

        if User.objects.filter(
            email__iexact=attrs["email"]
        ).exists():
            raise CustomAPIException(
                "Email Already in use.",
                1001
            )

        if StaffModel.objects.filter(
            ci__iexact=attrs["ci"]
        ).exists():
            raise CustomAPIException(
                "CI already register.",
                1002
            )

        if "type" in attrs:
            if attrs["type"] not in [type_s[0] for type_s in StaffModel.TYPES]:
                raise CustomAPIException(
                    "Type does not exist.",
                    1003
                )

        return attrs

    def create(self, validated_data):

        """
        This function handles al the creations for the Manager of a commerce.
        Using atomic transaction the creation can be rolled back if an error
        occurs
        :param validated_data: The validated data
        :return: data
        """

        try:
            username = validated_data["email"].lower()

            user_created = User.objects.create(
                username=username,
                email=validated_data["email"].lower(),
                password=make_password(validated_data["password"]),
                first_name=validated_data["first_name"],
                last_name=validated_data["last_name"],
                is_active=True,
            )
            user_created.set_password(validated_data["password"])
            user_created.save()

            # Profile Instance Model Creation
            # if
            StaffModel.objects.create(
                user=user_created,
                ci=validated_data["ci"],
                is_admin=validated_data["is_admin"] if "is_admin" in validated_data else False,
                type=validated_data["type"] if "type" in validated_data else StaffModel.TEACHER,
            )

        except Exception as err:
            raise CustomAPIException(
                "Something went wrong {}".format(err),
                1003
            )

        return user_created
