from rest_framework import serializers

from django.contrib.auth.models import User
from apps.user_profile.models import StaffModel


class UserSerializer(serializers.ModelSerializer):

    """
    Custom Serializer for User
    """

    class Meta:
        model = User
        fields = ("id", "first_name", "last_name", "email", "username")


class StaffSerializer(serializers.ModelSerializer):

    """
    Custom serializer for Staff with User
    """

    user = UserSerializer()

    class Meta:

        model = StaffModel
        fields = (
            # "id",
            "user",
            "is_admin",
            "ci",
            "type",
        )
