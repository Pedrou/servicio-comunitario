from .signup import SingUpSerializer
from .staff import StaffSerializer
from .actions import (
    CreateStaffActionSerializer,
    StaffActionsSerializer,
    ActionsSerializer,
    AttendanceStaffActionSerializer
)
from .staff_setting import StaffSettingSerializer

SingUpSerializer, StaffSerializer, CreateStaffActionSerializer, StaffActionsSerializer, \
    ActionsSerializer, StaffSettingSerializer, AttendanceStaffActionSerializer
