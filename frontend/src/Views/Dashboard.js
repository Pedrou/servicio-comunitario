import React, { Fragment } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import Divider from '@material-ui/core/Divider'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {Switch, Route, Redirect} from 'react-router-dom'
import { List } from '@material-ui/core';
import { PrimaryItems, SecondaryItems, ExtraItems } from '../components/MenuItems';
import { connect } from 'react-redux';
import Summary from './Dashboard/Summary';
import UserList from './Dashboard/UserList';
import Attendance from './Dashboard/Attendance';
import Register from './Dashboard/Register';
import AddStaff from './Dashboard/AddStaff';
import { logOut } from '../Store/Actions/Auth';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
}));

const Dashboard = (props) => {

  const pid = props.match.params.id
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const {user} = props

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const logOut = () => {
    props.logOut();
  }

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  if(!props.token)
    return(<Redirect to='/'/>)

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            Control de Asistencias
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          <PrimaryItems pathId={pid} isAdmin={user.isAdmin}/>
        </List>
        {user.isAdmin && <Fragment>
            <Divider/>
            <List>
            <SecondaryItems pathId={pid}/>
            </List>
        </Fragment>}
        <Divider/>
        <List>
          <ExtraItems pathId={pid} logOut={logOut}/>
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Switch>
            <Route exact path='/dashboard' component={user.isAdmin ? UserList : Summary}/>
            <Route path='/dashboard/attendance' component={user.isAdmin ? Attendance : null}/>
            <Route path='/dashboard/register' component={Register}/>
            <Route path='/dashboard/new-staff-member' component={AddStaff}/>
        </Switch>
      </main>
    </div>
  );
}

const mapStateToProps = (state) => {
    return ({
        user: state.Auth.userInfo,
        token: state.Auth.user.token
    })
}

const mapDispatchToProps = (dispatch) => {
  return ({
    logOut: () => {
      dispatch(logOut())
    }
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
