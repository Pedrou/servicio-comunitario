import React from 'react'
import { connect } from 'react-redux'
import { makeStyles, Typography } from '@material-ui/core'
import Mark from './Mark'
import {Link as RouterLink, Redirect} from 'react-router-dom'

const RLink = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);

const useStyles = makeStyles(theme=>({
    root: {
        minHeight: '100vh'
    },
    header: {
        background: theme.palette.primary.main,
        height: '30vh',
        color: 'white',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: '2.5em'
    },
    instructions: {
        fontSize: '1em'
    },
    content: {
        height: '65vh',
        color: 'black'
    },
    footer: {
        height: '5vh',
        background: '#CCC',
        color: '#707070',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    link:{
        color: '#707070',
        textDecoration: 'none'
    }
})) 

const RegisterMark = (props) => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <div className={classes.header}>
                <Typography className={classes.title}>Control de Asistencia</Typography>
                <Typography className={classes.instructions}>Ingrese su cedula y su contraseña para marcar entrada o salida</Typography>
            </div>
            <div className={classes.content}>
                <Mark/>
            </div>
            <div className={classes.footer}>
                <Typography className={classes.link} component={RLink} to='/login'>Iniciar Sesión</Typography>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return({

    })
}

const mapDispatchToProps = (dispatch) => {
    return({
        
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterMark)
