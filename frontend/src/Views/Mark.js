import React, { useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { connect } from 'react-redux';
import { LoginUser } from '../Store/Actions/Auth';
import {Link as RouterLink, Redirect} from 'react-router-dom'
import { markAttendance } from '../Store/Actions/staff';
import { Dialog, DialogTitle, DialogContent, DialogActions, DialogContentText } from '@material-ui/core';

const RLink = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);


const useStyles = makeStyles(theme => ({
  root:{
    minHeight: '50vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  error: {
    color: 'red',
    fontSize: '0.9em'
  }
}));

const Mark = (props) => {
  const classes = useStyles();
  const emailpattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const [open, setOpen] = React.useState(false);
  const [loginForm, setLoginForm] = React.useState({
    username: '',
    password: '',
  })

  useEffect(()=>{
    if(props.attendanceStatus === 'success' || props.attendanceStatus === 'failure')
        setOpen(true)
    if(props.attendanceStatus === 'success')
        setLoginForm({username: '', password: ''})
  }, [props.attendanceStatus])

  const handleChanges = (e) =>{
    setLoginForm({...loginForm, [e.target.id]: e.target.value})
  }


  const validateData = () =>{
    const {username, password} = loginForm
    const result = username
      && password.length >= 8
    return result;
  }

  const handleLogin = (e) =>{
    e.preventDefault();
    if(validateData()){
      props.markAttendance(loginForm)
    }
  }

  const handleClose = () => {
      setOpen(false)
  }

  if(props.userToken){
    console.log('jeje')
    return <Redirect to='/dashboard/'/>
  }
  let date
  let type
  if(props.attendancePayload && props.attendanceStatus === 'success'){
      date = new Date(props.attendancePayload.action.created);
      type = props.attendancePayload.action.type === 'arrival' ? 'llegada' : 'salida'
  }

  return (
    <Container component="main" maxWidth="xs" className={classes.root}>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Marca de Asistencia
        </Typography>
        {props.status === 'faillure' && <Typography className={classes.error}>Las credenciales introducidas no son correctas</Typography>}
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Cédula"
            name="username"
            autoComplete="username"
            value={loginForm.username}
            onChange={handleChanges}
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
            value={loginForm.password}
            onChange={handleChanges}
          />
        </form>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick = {handleLogin}
            disabled={!loginForm.username || loginForm.password.length < 8}
          >
            Marcar Registro
          </Button>
      </div>
      <Box mt={8}>
      </Box>
    {props.attendanceStatus === 'success' && <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle id="alert-dialog-title">{'Exito!'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {`Su ${type} fue registrada el ${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()} a las ${date.getHours() < 10 ? '0'+date.getHours() : date.getHours()}:${date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes()}`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>}
      {props.attendanceStatus === 'failure' && <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle id="alert-dialog-title">{'Error'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <Typography>Las credenciales ingresadas son incorrectas!</Typography>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>}
    </Container>
  );
}

const mapStateToProps = (state) => {
  return ({
    userToken: state.Auth.user.token,
    status: state.Auth.authStatus,
    attendancePayload: state.Staff.attendancePayload,
    attendanceStatus: state.Staff.attendanceStatus,
  })
}

const mapDispatchToProps = (dispatch) =>{
  return({
      markAttendance: (credentials) =>{
          dispatch(markAttendance(credentials))
      }
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(Mark)