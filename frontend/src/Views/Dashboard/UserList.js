import React, { useEffect } from 'react'
import { makeStyles, Paper, Typography, Button, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, Dialog } from '@material-ui/core'
import { connect } from 'react-redux'
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import Summary from './Summary';
import { GetUsers, generateReport } from '../../Store/Actions/staff';

const useStyles = makeStyles(theme=>({
    root: {
        padding: theme.spacing(4),
        minHeight: '80vh'
    },
    paper: {
        width: '100%',
        minHeight: '80vh',
        padding: theme.spacing(2)
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: theme.spacing(2,0)
    },
    clickeable: {
        cursor: 'pointer',
        transition: 'all ease 0.4s',
        '&:hover': {
            background: '#CCC'
        }
    },
    title: {
        fontWeight: 500,
        marginLeft: theme.spacing(2)
    },
    button: {
        margin: theme.spacing(1)
    }
}))

const UserList = (props) => {

    const {staff} = props
    const [user, setUser] = React.useState('')
    const [userInfo, setUserInfo] = React.useState('')
    const classes = useStyles();

    const [open, setOpen] = React.useState(false);

    useEffect(()=>{
        props.getUsers();
    },[])

    const cleanUser = () => {
        setUser('')
    }
    const handleClick = (id, ui) => (e) => {
        console.log('asdhjaksdhabsd', id, ui)
        setUser(id)
        setUserInfo(ui)
    }

    if(user !== ''){
        return(<Summary id={user} backFunction={cleanUser} user={userInfo}/>)
    }

    const handleClose = (e) => {
        setOpen(false)
    }

    const handleReportGeneration = (mode) => (e) => {
        props.generateReport(mode)
    }

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <div className={classes.header}>
                    <Typography className={classes.title}>Listado del Personal</Typography>
                    <Button onClick={handleReportGeneration(0)} className={classes.button} color='secondary' variant='contained'>Reporte de Hoy</Button>
                    <Button onClick={handleReportGeneration(1)} className={classes.button} color='secondary' variant='contained'>Reporte de la Semana</Button>
                    <Button onClick={handleReportGeneration(2)} className={classes.button} color='secondary' variant='contained'>Reporte del Mes</Button>
                    <Button onClick={handleReportGeneration(3)} className={classes.button} color='secondary' variant='contained'>Reporte Completo</Button>
                </div>
                    <TableContainer component={Paper}>
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <TableCell>C.I.</TableCell>
                                <TableCell align="right">Nombre</TableCell>
                                <TableCell align="right">Cargo</TableCell>
                                <TableCell align="right">Correo</TableCell>
                                <TableCell align="right">Administrador</TableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {staff.map(row => (
                                <TableRow key={row.ci} id={row.ci} onClick={handleClick(row.user.id, row.user)} className={classes.clickeable}>
                                <TableCell component="th" scope="row">
                                    {row.ci}
                                </TableCell>
                                <TableCell align="right">{`${row.user.first_name} ${row.user.last_name}`}</TableCell>
                                <TableCell align="right">{row.type}</TableCell>
                                <TableCell align="right">{row.user.email}</TableCell>
                                <TableCell align="right">{row.is_admin && <VerifiedUserIcon color='secondary'/>}</TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
            </Paper>
            {props.reportStatus === 'success' && <Dialog
                open={open}
                onClose={handleClose}
            >
                <DialogTitle id="alert-dialog-title">{'Exito!'}</DialogTitle>
                <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {`Su reporte fue generado con éxito`}
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary" autoFocus>
                    Ok
                </Button>
                </DialogActions>
            </Dialog>}

            {props.reportStatus === 'failure' && <Dialog
                open={open}
                onClose={handleClose}
            >
                <DialogTitle id="alert-dialog-title">{'Error'}</DialogTitle>
                <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {`Su reporte no pudo ser generado en este momento`}
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary" autoFocus>
                    Ok
                </Button>
                </DialogActions>
            </Dialog>}
        </div>
    )
}

const mapStateToProps = (state) => {
    return ({
        staff: state.Staff.staff,
        reportStatus: state.Staff
    })
}

const mapDispatchToProps = (dispatch) => {
    return ({
        getUsers: () => {
            dispatch(GetUsers());
        },
        generateReport: (mode) => {
            dispatch(generateReport(mode))
        }
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)
