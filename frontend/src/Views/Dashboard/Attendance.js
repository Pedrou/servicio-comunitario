import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { makeStyles, Paper, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Checkbox, Typography, Button, Tabs, Tab } from '@material-ui/core'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { GetUsers, markAction } from '../../Store/Actions/staff';

const useStyles = makeStyles(theme=>({
    root: {
        padding: theme.spacing(4)
    },
    paper: {
        padding: theme.spacing(2)
    },
    title: {
        fontSize: '1.1em',
        fontWeight: 800,
        marginBottom: theme.spacing(2)
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: theme.spacing(2,0)
    },
    buttonsWrapper: {
        display: 'flex',
        justifyContent: 'space-around'
    },
    error: {
        color: 'green',
        fontSize: '.9em'
    },
    error: {
        color: 'red',
        fontSize: '.9em'
    }
}))

const Attendance = (props) => {
    const classes = useStyles();
    const {staff} = props
    const [tab, setTab] = React.useState(0)
    const [checkboxes, setCheckboxes] = React.useState({})

    useEffect(()=>{
        props.getUsers();
    }, [])

    useEffect(()=>{
        let checkboxObject = {}
        staff.forEach(person=>{
            checkboxObject[person.ci] = false;
        })
        setCheckboxes(checkboxObject);
    }, [staff])

    const changeTab = (e, value) => {
        setTab(value);
    }

    const mark = (e) => {
        let assistance = [];
        staff.forEach(person=>{
            if(checkboxes[person.ci]){
                assistance.push(person.user.id)
            }
        })
        console.log(assistance, tab);
        if(tab === 0 || tab === 1)
            props.markAttendance(assistance, tab === 0 ? 'arrival' : 'departure')
    }

    const handleCheck = (e) => {
        let oldValues = {...checkboxes}
        oldValues[e.target.id] = e.target.checked;
        setCheckboxes(oldValues)
    }


    return (
        <div className={classes.root}>
            <Paper>
                <Tabs
                value={tab}
                onChange={changeTab}
                variant="fullWidth"
                centered
                >
                <Tab label="Entradas" icon={<ArrowForwardIcon/>}/>
                <Tab label="Salidas" icon={<ArrowBackIcon/>}/>
                </Tabs>
            </Paper>
            <Paper className={classes.paper}>
                {props.status === 'success' && <Typography className={classes.success}>El registro se guardo satisfactoriamente</Typography>}
                {props.status === 'failure' && <Typography className={classes.error}>Alguno de los registros seleccionados falló, revise que no esta guardando dos entradas o dos salidas seguidas para alguno de los usuarios seleccionados.</Typography>}
            <div className={classes.header}>
                    <Typography>Listado del Personal</Typography>
                    <Button onClick={mark} color='secondary' variant='contained'>{tab === 0 ? 'Marcar Entrada' : 'Marcar Salida'}</Button>
                </div>
                <TableContainer component={Paper}>
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <TableCell></TableCell>
                                <TableCell>C.I.</TableCell>
                                <TableCell align="right">Nombre</TableCell>
                                <TableCell align="right">Cargo</TableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {staff.map(row => (
                                <TableRow key={row.ci} id={row.ci}>
                                <TableCell><Checkbox id={row.ci} onChange={handleCheck} value={checkboxes[row.ci]}/></TableCell>
                                <TableCell component="th" scope="row">
                                    {row.ci}
                                </TableCell>
                                <TableCell align="right">{`${row.user.first_name} ${row.user.last_name}`}</TableCell>
                                <TableCell align="right">{row.type}</TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
            </Paper>
        </div>
    )
}

const mapStateToProps = (state) => {
    return ({
        staff: state.Staff.staff,
        status: state.Staff.markStatus
    })
}

const mapDispatchToProps = (dispatch) => {
    return ({
        getUsers: () => {
            dispatch(GetUsers());
        },
        markAttendance: (data, action) => {
            dispatch(markAction(data, action))
        }
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(Attendance)
