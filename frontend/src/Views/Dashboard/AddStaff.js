import React, { useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { connect } from 'react-redux';
import { RegisterUser } from '../../Store/Actions/Auth';
import { InputLabel, Select, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const AddStaff = (props) => {

  const classes = useStyles();
  const [open, setOpen] = React.useState(false)

  useEffect(()=>{
    if(props.status === 'success' || props.status === 'failure')
    setOpen(true)
    if(props.status === 'success')
    setRegisterForm({
      email: '',
      password: '',
      passwordConfirm: '',
      type: 'none',
      first_name: '',
      last_name: '',
      ci: '',
    })
  }, [props.status])

  const [registerForm, setRegisterForm] = React.useState({
    email: '',
    password: '',
    passwordConfirm: '',
    type: 'none',
    first_name: '',
    last_name: '',
    ci: '',
  })
  const [errors, setErrors] = React.useState({
    email: false,
    password: false,
    passwordConfirm: false,
    type: 'teacher',
    first_name: false,
    last_name: false,
    ci: false,
  })

  useEffect(()=>{
    validateData();
  },[registerForm])

  const handleClose = () => {
    setOpen(false);
  }

  const handleChanges = (e) =>{
    const newValues = {...registerForm}
    newValues[e.target.id] = e.target.value;
    setRegisterForm(newValues);
  }
  const handleChange = (e) =>{
    const newValues = {...registerForm}
    newValues.type = e.target.value
    setRegisterForm(newValues);
  }

  const validateData = () =>{
    const {email, password, passwordConfirm, first_name, last_name, ci, type} = registerForm

    const emailpattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const result = first_name && last_name && ci && (type === 'director' || type === 'teacher' || type === 'administrative_personal' || type === 'coordinator') 
      && emailpattern.test(email)
      && password.length >= 8 && password === passwordConfirm
    console.log(registerForm, '==', result)
    let newErrors = {
      email: false,
      password: false,
      passwordConfirm: false,
      type: false,
      first_name: false,
      last_name: false,
      ci: false,
    }
    if(!result){
      console.log("TESTEANDO ****", passwordConfirm, password === passwordConfirm, password)
      newErrors = {
        email: !email || !emailpattern.test(email),
        password: !password || password.length < 8,
        passwordConfirm: !passwordConfirm || passwordConfirm !== password,
        first_name: !first_name,
        last_name: !last_name,
        ci: !ci,
        type: !(type === 'director' || type === 'teacher' || type === 'administrative_personal' || type === 'coordinator')
      }
    }
    setErrors(newErrors)
    return {result, newErrors};
  }

  const handleRegister = (e) =>{
    e.preventDefault();

    if(validateData().result){
      props.registerUser(registerForm)
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        {props.status === 'success' && <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle id="alert-dialog-title">{'Exito!'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {`Usuario Registrado Exitosamente`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>}
        {props.status === 'failure' && <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle id="alert-dialog-title">{'Error'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {`Error al registrar el usuario: ${props.registerError.error.message}`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>}
        <Typography component="h1" variant="h5">
          Registro de Usuarios
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoComplete="uname"
                name="first_name"
                variant="outlined"
                required
                fullWidth
                id="first_name"
                label="Nombre"
                autoFocus
                onChange={handleChanges}
                value={registerForm.first_name}
                helperText={errors.first_name ? 'Este campo es requerido' : ''}
                error={errors.first_name}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name="last_name"
                variant="outlined"
                required
                fullWidth
                id="last_name"
                label="Apellido"
                onChange={handleChanges}
                value={registerForm.last_name}
                helperText={errors.last_name ? 'Este campo es requerido' : ''}
                error={errors.last_name}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name="email"
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Correo"
                onChange={handleChanges}
                value={registerForm.email}
                helperText={errors.email ? 'El correo no es un correo válido' : ''}
                error={errors.email}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name="ci"
                variant="outlined"
                required
                fullWidth
                id="ci"
                label="Cédula"
                onChange={handleChanges}
                value={registerForm.ci}
                helperText={errors.ci ? 'Este campo es requerido' : ''}
                error={errors.ci}
              />
            </Grid>
            <Grid item xs={12}>
                <InputLabel id="demo-simple-select-label">Cargo</InputLabel>
                <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={registerForm.type}
                onChange={handleChange}
                fullWidth
                error={errors.type}
                >
                    <MenuItem value={'none'}>-- Seleccione una opción --</MenuItem>
                    <MenuItem value={'teacher'}>Maestro</MenuItem>
                    <MenuItem value={'director'}>Director</MenuItem>
                    <MenuItem value={'coordinator'}>Coordinador</MenuItem>
                    <MenuItem value={'administrative_personal'}>Personal Administrativo</MenuItem>
                </Select>
            </Grid>

            <Grid item xs={12}>
              <TextField
                name="password"
                variant="outlined"
                type='password'
                required
                fullWidth
                id="password"
                label="Contraseña"
                onChange={handleChanges}
                value={registerForm.password}
                helperText={errors.password ? 'La contraseña debe tener al menos 8 caracteres.' : ''}
                error={errors.password}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name="passwordConfirm"
                variant="outlined"
                type='password'
                required
                fullWidth
                id="passwordConfirm"
                label="Confirmar Contraseña"
                onChange={handleChanges}
                value={registerForm.passwordConfirm}
                helperText={errors.passwordConfirm ? 'Las contraseñas no son iguales' : ''}
                error={errors.passwordConfirm}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleRegister}
            disabled={errors.first_name || errors.last_name || errors.email || errors.ci || errors.passwordConfirm || errors.password || errors.type}
          >
            Registrar Usuario
          </Button>
        </form>
      </div>
      <Box mt={5}>
      </Box>
    </Container>
  );
}

const mapStateToProps = (state) => {
    return ({
        registerError: state.Auth.registerError,
        status: state.Auth.registerStatus,
    })
}

const mapDispatchToProps = (dispatch) =>{
    return({
        registerUser: (data) =>{
            dispatch(RegisterUser(data))
        }
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(AddStaff)