import React, { useEffect } from 'react'
import { makeStyles, Paper, Button, Typography, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core'
import { connect } from 'react-redux'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { getActions } from '../../Store/Actions/staff';
import { ExportToCsv } from 'export-to-csv';

const useStyles = makeStyles(theme=>({
    root: {
        padding: theme.spacing(4),
        minHeight: '80vh'
    },
    paper: {
        width: '100%',
        minHeight: '80vh',
        padding: theme.spacing(2)
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: theme.spacing(2,0)
    },
    arrival: {
        display: 'flex',
        alignItems: 'center',
        color: 'green'
    },
    departure: {
        display: 'flex',
        alignItems: 'center',
        color: 'red'
    },
    title: {
        fontWeight: 500,
        marginLeft: theme.spacing(2)
    },
    backButton: {
        margin: theme.spacing(0,0,2,0)
    }
}))



const Summary = (props) => {
    const {attendance, id, backFunction, stateUser, user} = props
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    useEffect(()=>{
        props.getActions(id ? id : stateUser.id)
    }, [])

    const generateCSV = () => {
        console.log(user, stateUser)
        const options = { 
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalSeparator: '.',
            showLabels: true, 
            showTitle: true,
            title: `Reporte Asistencias: ${user ? user.first_name + " " + user.last_name : stateUser.name}`,
            useTextFile: false,
            useBom: true,
            useKeysAsHeaders: false,
            headers: ['Acción', 'Fecha', 'Hora']
          };
        let objects = []
        attendance.forEach(a=>{
            let date = new Date(a.created)
            objects.push({
                type: a.type === 'arrival' ? 'Llegada' : 'Salida',
                date: `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`,
                time: `${date.getHours() < 10 ? '0'+date.getHours() : date.getHours()}:${date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes()}`
            })
        })
        const csvExporter = new ExportToCsv(options);
        csvExporter.generateCsv(objects);
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false);
    }

    return (
        <div className={classes.root}>
            {id && <Button className={classes.backButton} onClick={backFunction}><ArrowBackIcon/><Typography>Atrás</Typography></Button>}
            <Paper className={classes.paper}>
                <div className={classes.header}>
                    <Typography className={classes.title}>Registro de Asistencias</Typography>
                    <Button color='secondary' variant='contained' onClick={generateCSV}>Exportar</Button>
                </div>
                    <TableContainer component={Paper}>
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <TableCell>Acción</TableCell>
                                <TableCell align="right">Fecha</TableCell>
                                <TableCell align="right">Hora</TableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {attendance.map(row => {
                                let date = new Date(row.created);
                                return(
                                <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    <Typography className={row.type === 'arrival' ? classes.arrival : classes.departure}>{row.type === 'arrival' ? <ArrowForwardIcon/> : <ArrowBackIcon/>}
                                    {row.type === 'arrival' ? 'Entrada' : 'Salida'}</Typography>
                                </TableCell>
                                <TableCell align="right">{`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`}</TableCell>
                                <TableCell align="right">{`${date.getHours() < 10 ? '0'+date.getHours() : date.getHours()}:${date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes()}`}</TableCell>
                                </TableRow>
                            )})}
                            </TableBody>
                        </Table>
                    </TableContainer>
            </Paper>
            <Dialog
                open={open}
                onClose={handleClose}
            >
                <DialogTitle id="alert-dialog-title">{'Exito!'}</DialogTitle>
                <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {`Su reporte fue generado con éxito`}
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary" autoFocus>
                    Ok
                </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

const mapStateToProps = (state) => {
    return ({
        attendance: state.Staff.attendance,
        stateUser: state.Auth.userInfo
    })
}

const mapDispatchToProps = (dispatch) => {
    return ({
        getActions: (user) => {
            dispatch(getActions(user))
        }
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(Summary)
