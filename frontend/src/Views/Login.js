import React, { Fragment } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { connect } from 'react-redux';
import { LoginUser } from '../Store/Actions/Auth';
import {Link as RouterLink, Redirect} from 'react-router-dom'

const RLink = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);

const useStyles = makeStyles(theme => ({
  root:{
    minHeight: '95vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  error: {
    color: 'red',
    fontSize: '0.9em'
  },
  footer: {
    height: '5vh',
    background: '#CCC',
    color: '#707070',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  link:{
      color: '#707070',
      textDecoration: 'none'
  }
}));

const Login = (props) => {
  const classes = useStyles();
  const emailpattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const [loginForm, setLoginForm] = React.useState({
    username: '',
    password: '',
  })

  const handleChanges = (e) =>{
    setLoginForm({...loginForm, [e.target.id]: e.target.value})
  }

  const validateData = () =>{
    const {username, password} = loginForm
    const result = emailpattern.test(username)
      && password.length >= 8
    return result;
  }

  const handleLogin = (e) =>{
    e.preventDefault();
    if(validateData()){
      props.loginUser(loginForm)
    }
  }

  if(props.userToken){
    console.log('jeje')
    return <Redirect to='/dashboard/'/>
  }

  return (
    <Fragment>
    <Container component="main" maxWidth="xs" className={classes.root}>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Autenticación
        </Typography>
        {props.status === 'faillure' && <Typography className={classes.error}>Las credenciales introducidas no son correctas</Typography>}
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Correo"
            name="username"
            autoComplete="username"
            value={loginForm.username}
            onChange={handleChanges}
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
            value={loginForm.password}
            onChange={handleChanges}
          />
        </form>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick = {handleLogin}
            disabled={!emailpattern.test(loginForm.username) || loginForm.password.length < 8}
          >
            Iniciar Sesión
          </Button>
      </div>
      <Box mt={8}>
      </Box>
    </Container>
    <div className={classes.footer}>
        <Typography className={classes.link} component={RLink} to='/'>Marcar Asistencia</Typography>
    </div>
    </Fragment>
  );
}

const mapStateToProps = (state) => {
  return ({
    userToken: state.Auth.user.token,
    status: state.Auth.authStatus
  })
}

const mapDispatchToProps = (dispatch) =>{
  return({
      loginUser: (credentials) =>{
          dispatch(LoginUser(credentials))
      }
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)