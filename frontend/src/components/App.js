import React, { Component } from 'react'
import {Typography, createMuiTheme, ThemeProvider} from '@material-ui/core'
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import RootReducer from '../Store/Reducers/RootReducer'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import Login from '../Views/Login';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Dashboard from '../Views/Dashboard';
import RegisterMark from '../Views/RegisterMark';

const initialState = {}
const middleware = [thunk]
const Store = createStore(RootReducer, initialState, applyMiddleware(...middleware))

const Theme = createMuiTheme({
    palette: {
      primary: {
        main: '#0A2239',
        light: '#53A2BE',
        dark: '#0055a0'
      },
      secondary: {
        main: '#ff5722',
        dark: '#922503'
  
      },
      biology: {
        main: '#FF0000'
      }
    },
    typography: {
      fontFamily: [
        'Merriweather',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
    },
  });

class App extends Component {
    render() {
        return (
        <div className='App'>
            <BrowserRouter>
                <ThemeProvider theme={Theme}>
                <Switch>
                    <Route exact path='/' component={RegisterMark}/>
                    <Route exact path='/login' component={Login}/>
                    <Route path='/dashboard' component={Dashboard}/>
                </Switch>
                </ThemeProvider>
            </BrowserRouter>
        </div>
        )
    }
}

ReactDOM.render(<Provider store={Store}><App/></Provider>, document.getElementById('app'))
