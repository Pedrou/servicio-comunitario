import EqualizerIcon from '@material-ui/icons/Equalizer';
import SchoolIcon from '@material-ui/icons/School';
import VisibilityIcon from '@material-ui/icons/Visibility';
import React, { Fragment } from 'react';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import {Link as RouterLink} from 'react-router-dom'
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import AddIcon from '@material-ui/icons/Add';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';

const Link = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);

export const PrimaryItems = (props) =>{
    return (
        <Fragment>
            <ListItem button component={Link} to={`/dashboard`}>
            <ListItemIcon>
                <EqualizerIcon/>
            </ListItemIcon>
            <ListItemText>
                Reporte
            </ListItemText>
            </ListItem>
            {props.isAdmin && <Fragment>
                <ListItem button component={Link} to={`/dashboard/attendance`}>
                <ListItemIcon>
                    <CheckBoxIcon/>
                </ListItemIcon>
                <ListItemText>
                    Asistencia
                </ListItemText>
                </ListItem>
                <ListItem button component={Link} to={`/dashboard/new-staff-member`}>
                <ListItemIcon>
                    <AddIcon/>
                </ListItemIcon>
                <ListItemText>
                    Nuevo Personal
                </ListItemText>
                </ListItem>
            </Fragment>}
        </Fragment>
    )
}



export const SecondaryItems = (props) =>{
    return (
        <Fragment>
            <ListItem button component={Link} to={`/dashboard/register`}>
            <ListItemIcon>
                <SupervisorAccountIcon/>
            </ListItemIcon>
            <ListItemText>
                Registrar Ususario
            </ListItemText>
            </ListItem>
        </Fragment>)

}

export const ExtraItems = (props) =>{
    return (
        <Fragment>
            <ListItem button onClick={props.logOut}>
            <ListItemIcon>
                <ExitToAppIcon/>
            </ListItemIcon>
            <ListItemText>
                Cerrar Sesión
            </ListItemText>
            </ListItem>
        </Fragment>)

}