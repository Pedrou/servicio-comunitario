import { GETTING_USERS, SET_STAFF, MARK_ATTEMPT, MARK_SUCCESS, MARK_FAILURE, SET_ATTENDANCE, ATTENDANCE_ATTEMPT, ATTENDANCE_SUCCESS, ATTENDANCE_FAILURE, REPORT_ATTEMPT } from "../types"
import Axios from "axios"
import { ExportToCsv } from 'export-to-csv';


export const GetUsers = () =>{
    return (dispatch, getState) =>{
        dispatch({type: GETTING_USERS})
        const headers = {
            Authorization: `Token ${getState().Auth.user.token}`
        }
        Axios.get('/api/staff/', {headers: headers}).then(r=>{
            console.log(r.data)
            dispatch({type: SET_STAFF, payload: r.data})
        }).catch(e=>{
            console.log(e);
        })
    }
}

export const markAction = (users, action) => {
    return (dispatch, getState) => {
        dispatch({type: MARK_ATTEMPT});
        const headers = {
            Authorization: `token ${getState().Auth.user.token}`
        }
        let promises = []
        users.forEach(mark=>{
            promises.push(Axios.post('/api/staff_action/', {staff: mark, action: action}, {headers}))
        })
        Promise.all(promises).then(responses => {
            dispatch({type: MARK_SUCCESS})
        }).catch(error=> {
            dispatch({type: MARK_FAILURE})
        })
    }
}

export const getActions = (user) => {
    return (dispatch, getState) => {
        const id = user !== null ? user : getState().Auth.userInfo.id
        const headers = {
            Authorization: `token ${getState().Auth.user.token}`
        }
        Axios.get('/api/staff_action/list/?user='+id, {headers}).then(res=>{
            console.log(res)
            let actions = []
            res.data.forEach(action=>{
                actions.unshift(action.action)
            })
            dispatch({type: SET_ATTENDANCE, payload: actions})
        }).catch(err=>{
            console.log(err.response)
        })

    }
}

export const markAttendance = (credentials) => {
    return (dispatch, getState) => {
        dispatch({type: ATTENDANCE_ATTEMPT})
        console.log(credentials);
        Axios.post(`/api/attendance/`, {ci: credentials.username, password: credentials.password}).then(response=>{
            dispatch({type: ATTENDANCE_SUCCESS, payload: response.data})
            console.log(response.data)
        }).catch(err=>{
            console.log(err.response)
            dispatch({type: ATTENDANCE_FAILURE, payload: err.response})
        })
    }
}

export const generateReport = (mode) => {
    return (dispatch, getState) => {
        let fromDate = null;
        let toDate = null;
        let today = new Date();
        const headers = {
            Authorization: `token ${getState().Auth.user.token}`
        }
        if(mode === 0){
            //Hoy
            fromDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0 ,0 ,0)
            toDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 23, 59 ,59 , 999)
        }else if(mode === 1){
            //Esta semana
            let weekStart = new Date(today.setDate(today.getDate()-today.getDay()))
            let weekEnd = new Date(today.setDate(today.getDate()+6-today.getDay()))
            fromDate = new Date(weekStart.getFullYear(), weekStart.getMonth(), weekStart.getDate(), 0, 0, 0, 0)
            toDate = new Date(weekEnd.getFullYear(), weekEnd.getMonth(), weekEnd.getDate(), 23, 59, 59, 999)
        }else if(mode === 2){
            //Este mes
            fromDate = new Date(today.getFullYear(), today.getMonth(),0, 0, 0 ,0 ,0)
            toDate = new Date(today.getFullYear(), today.getMonth()+1, 0, 0, 0 ,0 , 0)
        }else if(mode === 3){
            //Todo
        }
        dispatch({type: REPORT_ATTEMPT})
        let promise
        if(toDate !== null){
           promise = Axios.get(`/api/staff_action/list/?from=${fromDate.toISOString()}&&to=${toDate.toISOString()}`, {headers})
        }else{
           promise = Axios.get(`/api/staff_action/list/`, {headers})
        }
        promise.then(response=>{
            console.log('response', response);
            const options = { 
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalSeparator: '.',
                showLabels: true, 
                showTitle: true,
                title: `Reporte de asistencias: ${mode === 0 ? 'Hoy' : mode === 1 ? 'Esta Semana' : mode === 2 ? 'Este Mes' : 'Todos los Registros'}`,
                useTextFile: false,
                useBom: true,
                useKeysAsHeaders: false,
                headers: ['C.I', 'Nombre', 'email', 'Acción', 'Fecha', 'Hora']
              };
            let objects = []
            response.data.forEach(a=>{
                let date = new Date(a.action.created)
                objects.push({
                    ci: a.staff.ci,
                    name: a.staff.user.first_name + " " + a.staff.user.last_name,
                    email: a.staff.user.email + " " + a.staff.user.email,
                    type: a.action.type === 'arrival' ? 'Llegada' : 'Salida',
                    date: `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`,
                    time: `${date.getHours() < 10 ? '0'+date.getHours() : date.getHours()}:${date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes()}`
                })
            })
            const csvExporter = new ExportToCsv(options);
            csvExporter.generateCsv(objects);
        }).catch(err=>{
            console.log('response', err.response);    
        })
    }
}
