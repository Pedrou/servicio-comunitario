import { LOGING_IN, SET_USER, REGISTER_ATTEMPT, REGISTER_FAILURE, REGISTER_SUCCESS, AUTH_FAIULLURE, LOG_OUT } from "../types"
import Axios from "axios"

export const LoginUser = (credentials) =>{
    return (dispatch) =>{
        const {username, password} = credentials
        dispatch({type: LOGING_IN})
        console.log('Hola')
        Axios.post('/api/auth/', {username, password}).then(r=>{
            console.log('listo', r.data)
            dispatch({type: SET_USER, payload: 
                {userInfo: {email: r.data.email, name: r.data.name, isAdmin: r.data.is_admin, id: r.data.id}, 
                user: {token: r.data.token},
            }})
        }).catch(e=>{
            console.log(e);
            dispatch({type: AUTH_FAIULLURE})
        })
    }
}

export const RegisterUser = (data, is_admin = false) =>{
    return (dispatch) =>{
        const {first_name, last_name, email, password, ci, type} = data
        dispatch({type: REGISTER_ATTEMPT})
        Axios.post('/api/signup/', {first_name, last_name, email, password, ci, type, is_admin}).then(r=>{
            dispatch({type: REGISTER_SUCCESS})
        }).catch(err=>{
            console.log(err.response);
            dispatch({type: REGISTER_FAILURE, payload: err.response.data})
        })
    }
}

export const logOut = () => {
    return (dispatch) => {
        dispatch({type: LOG_OUT})
    }
}