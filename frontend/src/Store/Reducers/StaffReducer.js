import { SET_STAFF, MARK_FAILURE, MARK_ATTEMPT, MARK_SUCCESS, SET_ATTENDANCE, ATTENDANCE_SUCCESS, ATTENDANCE_FAILURE, ATTENDANCE_ATTEMPT } from '../types.js'

const initialState = {
    staff: [],
    markStatus: 'none',
    attendance: [],
    attendanceStatus: 'none',
    attendancePayload: null
}

const StaffReducer = (state = initialState, action) =>{
    let newState = {...state}
    switch(action.type){
        case SET_STAFF: 
            newState.staff = action.payload;
            return newState;
        case MARK_FAILURE:
            newState.markStatus = 'failure';
            return newState
        case MARK_ATTEMPT:
            newState.markStatus = 'loading';
            return newState
        case MARK_SUCCESS:
            newState.markStatus = 'success';
            return newState
        case SET_ATTENDANCE:
            newState.attendance = action.payload
            return newState;
        case ATTENDANCE_ATTEMPT:
            newState.attendanceStatus = 'loading';
            newState.attendancePayload = null;
            return newState;
        case ATTENDANCE_SUCCESS:
            newState.attendanceStatus = 'success';
            newState.attendancePayload = action.payload;
            return newState;
        case ATTENDANCE_FAILURE:
            newState.attendanceStatus = 'failure';
            newState.attendancePayload = action.payload;
            return newState;
        default:
            return newState
    }
}

export default StaffReducer