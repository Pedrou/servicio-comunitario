import { LOGING_IN, SET_USER, REGISTER_ATTEMPT, REGISTER_SUCCESS, REGISTER_FAILURE, AUTH_FAIULLURE, LOG_OUT } from '../types.js'

const initialState = {
    user: {token: ''},
    userInfo: {name: '', email: '', isAdmin: true},
    loading: false,
    registerStatus: 'none',
    registerError: null,
    authStatus: 'none',
}

const AuthReducer = (state = initialState, action) =>{
    let newState = {...state}
    switch(action.type){
        case LOGING_IN: 
            newState.authStatus = 'loading'
            return newState;
        case SET_USER:
            newState.user = action.payload.user;
            newState.userInfo = action.payload.userInfo;
            newState.authStatus = 'success'
            return newState;
        case AUTH_FAIULLURE:
            newState.authStatus = 'faillure'
        case REGISTER_ATTEMPT:
            newState.registerStatus = 'loading';
            newState.registerStatus = null
            return newState;
        case REGISTER_SUCCESS:
            newState.registerStatus = 'success';
            return newState;
        case REGISTER_FAILURE:
            newState.registerStatus = 'failure';
            newState.registerError = action.payload
            return newState;
        case LOG_OUT:
            console.log('entrando aca de alguna forma')
            newState = initialState;
            return newState;
        default:
            return newState
    }
}

export default AuthReducer