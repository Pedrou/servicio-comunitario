import {combineReducers} from 'redux'
import AuthReducer from './AuthReducer'
import StaffReducer from './StaffReducer'

const RootReducer = combineReducers({
    Auth: AuthReducer,
    Staff: StaffReducer
});

export default RootReducer;